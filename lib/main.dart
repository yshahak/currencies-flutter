import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'RatesBLoC.dart';
import 'RatesWrapper.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Currencies',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Currencies'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key) {
    myController.addListener(_onChange);
  }

  final String title;
  final myController = TextEditingController();
  final RatesBLoC ratesBLoC = RatesBLoC();
  Header header;
  RateModel rateModel;

  _onChange() {
    header.changeFactor(double.parse(myController.text));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  StreamBuilder<Header>(
                      stream: ratesBLoC.getHeaderStream,
                      initialData: Header.empty(),
                      builder: (context, snapshot) {
                        this.header = snapshot.data;
                        myController.text = this.header.factor.toStringAsFixed(2);
                        return ListTile(
                          leading: Image(image: AssetImage("assets/${labelToIconMap[this.header.base]}")),
                          contentPadding: const EdgeInsets.all(8),
                          title: Text(this.header.base),
                          subtitle: Text(mapRes[labelToDisplayNameMap[this.header.base]]),
                          trailing: Container(
                            width: 150,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Expanded(
                                  child: TextField(
                                    textAlign: TextAlign.end,
                                    controller: myController,
                                    keyboardType: TextInputType.number,
                                    textInputAction: TextInputAction.done,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                  StreamBuilder<RateModel>(
                      stream: ratesBLoC.getRatesStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          this.rateModel = snapshot.data;
                          var list = this.rateModel.rates.entries.toList();
                          var children = list.map((entry) => _buildItem(entry.key, entry.value, () => ratesBLoC.onLoadMoreSubjectClick.add(entry)))?.toList();
                          return Column(children: children);
                        } else {
                          return Container();
                        }
                      })
                ],
              ),
            ),
          ),
        ));

  }

  Widget _buildItem(String key, double value, [void Function() onTapCallback]) {
    return ListTile(
      contentPadding: const EdgeInsets.all(8),
      leading: Image(image: AssetImage("assets/${labelToIconMap[key]}")),
      subtitle: Text(mapRes[labelToDisplayNameMap[key]]),
      title: Text(key),
      trailing: Text((value * this.header.factor).toStringAsFixed(2)),
      onTap: onTapCallback,
    );
  }
}

var labelToIconMap = {
  "AUD": 'ic_aud_flag.png',
  "BGN": 'ic_bgn_flag.png',
  "BRL": 'ic_brl_flag.png',
  "CAD": 'ic_cad_flag.png',
  "CHF": 'ic_chf_flag.png',
  "CNY": 'ic_cny_flag.png',
  "CZK": 'ic_czk_flag.png',
  "DKK": 'ic_dkk_flag.png',
  "EUR": 'ic_eur_flag.png',
  "GBP": 'ic_gbp_flag.png',
  "HKD": 'ic_hkd_flag.png',
  "HRK": 'ic_hrk_flag.png',
  "HUF": 'ic_huf_flag.png',
  "IDR": 'ic_idr_flag.png',
  "ILS": 'ic_ils_flag.png',
  "INR": 'ic_inr_flag.png',
  "ISK": 'ic_isk_flag.png',
  "JPY": 'ic_jpy_flag.png',
  "KRW": 'ic_krw_flag.png',
  "MXN": 'ic_mxn_flag.png',
  "MYR": 'ic_myr_flag.png',
  "NOK": 'ic_nok_flag.png',
  "NZD": 'ic_nzd_flag.png',
  "PHP": 'ic_php_flag.png',
  "PLN": 'ic_pln_flag.png',
  "RON": 'ic_ron_flag.png',
  "RUB": 'ic_rub_flag.png',
  "SEK": 'ic_sek_flag.png',
  "SGD": 'ic_sgd_flag.png',
  "THB": 'ic_thb_flag.png',
  "TRY": 'ic_try_flag.png',
  "USD": 'ic_usd_flag.png',
  "ZAR": 'ic_zar_flag.png'
};

var labelToDisplayNameMap = {
  "AUD": "currency_aud_name",
  "BGN": "currency_bgn_name",
  "BRL": "currency_brl_name",
  "CAD": "currency_cad_name",
  "CHF": "currency_chf_name",
  "CNY": "currency_cny_name",
  "CZK": "currency_czk_name",
  "DKK": "currency_dkk_name",
  "EUR": "currency_eur_name",
  "GBP": "currency_gbp_name",
  "HKD": "currency_hkd_name",
  "HRK": "currency_hrk_name",
  "HUF": "currency_huf_name",
  "IDR": "currency_idr_name",
  "ILS": "currency_ils_name",
  "INR": "currency_inr_name",
  "ISK": "currency_isk_name",
  "JPY": "currency_jpy_name",
  "KRW": "currency_krw_name",
  "MXN": "currency_mxn_name",
  "MYR": "currency_myr_name",
  "NOK": "currency_nok_name",
  "NZD": "currency_nzd_name",
  "PHP": "currency_php_name",
  "PLN": "currency_pln_name",
  "RON": "currency_ron_name",
  "RUB": "currency_rub_name",
  "SEK": "currency_sek_name",
  "SGD": "currency_sgd_name",
  "THB": "currency_thb_name",
  "TRY": "currency_try_name",
  "USD": "currency_usd_name",
  "ZAR": "currency_zar_name"
};

var mapRes = {
  "currency_aud_name": "Australian dollar",
  "currency_bgn_name": "Bulgarian lev",
  "currency_brl_name": "Brazilian real",
  "currency_cad_name": "Canadian dollar",
  "currency_chf_name": "Swiss franc",
  "currency_cny_name": "Chinese yuan",
  "currency_czk_name": "Czech koruna",
  "currency_dkk_name": "Danish krone",
  "currency_eur_name": "Euro",
  "currency_gbp_name": "British pound",
  "currency_hkd_name": "Hong Kong dollar",
  "currency_hrk_name": "Croatian kuna",
  "currency_huf_name": "Hungarian forint",
  "currency_idr_name": "Indonesian rupiah",
  "currency_ils_name": "Israeli new shekel",
  "currency_inr_name": "Indian rupee",
  "currency_isk_name": "Icelandic króna",
  "currency_jpy_name": "Japanese yen",
  "currency_krw_name": "South Korean won",
  "currency_mxn_name": "Mexican peso",
  "currency_myr_name": "Malaysian ringgit",
  "currency_nok_name": "Norwegian krone",
  "currency_nzd_name": "New Zealand dollar",
  "currency_php_name": "Philippine piso",
  "currency_pln_name": "Polish złoty",
  "currency_ron_name": "Romanian leu",
  "currency_rub_name": "Russian ruble",
  "currency_sek_name": "Swedish krona",
  "currency_sgd_name": "Singapore dollar",
  "currency_thb_name": "Thai baht",
  "currency_try_name": "Turkish lira",
  "currency_usd_name": "United States dollar",
  "currency_zar_name": "South African rand"
};
