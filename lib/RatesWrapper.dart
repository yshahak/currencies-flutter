class Header  {
  String base = "EUR";
  double factor = 1;


  Header(this.base, this.factor);

  factory Header.empty(){
    return Header("EUR", 1);
  }

  void changeFactor(double newFactor){
    if (newFactor != factor){
      factor = newFactor;
    }
  }
}

class RateModel {
  final String base;
  final String date;
  final Map<String, double> rates;
  RateModel({this.base, this.date, this.rates});

  factory RateModel.fromJson(Map<String, dynamic> json) {
    return RateModel(
        base: json['base'],
        date: json['date'],
        rates: (json['rates'] as Map).map((key, value) => MapEntry(key as String, value as double))
    );
  }
}


