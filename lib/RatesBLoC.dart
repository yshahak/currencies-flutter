import 'dart:async';
import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;
import 'RatesWrapper.dart';

class RatesBLoC {
  final _ratesSubject = BehaviorSubject<RateModel>();
  final _headerSubject = BehaviorSubject<Header>();
  final _onLoadMoreSubject = StreamController<MapEntry<String, double>>();
  var _header = Header.empty();

  Stream<RateModel> get getRatesStream => _ratesSubject.stream;

  Stream<Header> get getHeaderStream => _headerSubject.stream;

  Sink<MapEntry<String, double>> get onLoadMoreSubjectClick => _onLoadMoreSubject.sink;

  RatesBLoC() {
    _headerSubject.add(_header);
    Future.doWhile(() async {
      await fetchRates(_header.base);
      await Future.delayed(new Duration(seconds: 1));
      return true;
    });

    _onLoadMoreSubject.stream.listen((entry) async {
      _header = Header(entry.key, _header.factor * entry.value);
      _headerSubject.add(_header);
      await fetchRates(_header.base);
    });
  }

  Future fetchRates(String base) async {
    final response = await http.get('https://revolut.duckdns.org/latest?base=$base');
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      _ratesSubject.add(RateModel.fromJson(json.decode(response.body)));
    }
  }

  void close() {
    _ratesSubject.close();
    _headerSubject.close();
    _onLoadMoreSubject.close();
  }
}
